package serie03;

import javax.swing.*;
import java.awt.*;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gpyh on 2/11/16.
 */
public class ClientFTP {

    private static final String ASCII_LABEL = "ASCII";
    private static final String BINARY_LABEL = "Binary";
    private static final String AUTO_LABEL = "Auto";
    private static final int PANEL_WIDTH = 300;
    private static final int PANEL_HEIGHT = 300;
    private static final int CONSOLE_WIDTH = 300;
    private static final int CONSOLE_HEIGHT = 70;

    private JFrame mainFrame;
    private JTextArea console;
    private JPanel localPanel;
    private JPanel remotePanel;
    private JButton remoteToLocal;
    private JButton localToRemote;
    private JRadioButton asciiRadio;
    private JRadioButton binaryRadio;
    private JCheckBox autoBox;
    private Map<StatusAction, JButton> statusButtons;

    private enum StatusAction {
        CONNECT("Connect"),
        CANCEL("Cancel"),
        LOGWND("LogWnd"),
        HELP("Help"),
        OPTIONS("Options"),
        ABOUT("About"),
        EXIT("Exit");

        private String label;

        StatusAction(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    public ClientFTP() {
        createModel();
        createView();
        placeComponents();
        createController();
    }

    private void createModel() {
        // NOTHING
    }

    private void createView() {
        mainFrame = new JFrame();

        console = new JTextArea();
        console.setPreferredSize(new Dimension(CONSOLE_WIDTH, CONSOLE_HEIGHT));

        localPanel = new FilePanel("Local System", "/home/gpyh");
        remotePanel = new FilePanel("Remote System", "/home/gpyh");
        //localPanel = new JPanel();
        //localPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        //localPanel.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

        //remotePanel = new JPanel();
        //remotePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        //remotePanel.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

        remoteToLocal = new JButton(" <- ");
        localToRemote = new JButton(" -> ");

        asciiRadio = new JRadioButton(ASCII_LABEL);
        binaryRadio = new JRadioButton(BINARY_LABEL);
        autoBox = new JCheckBox(AUTO_LABEL);

        statusButtons = new EnumMap<StatusAction, JButton>(StatusAction.class);
        for(StatusAction sa : StatusAction.values()) {
            statusButtons.put(sa, new JButton(sa.toString()));
        }
    }

    private void placeComponents() {
        JPanel p = new JPanel(new GridBagLayout()); {
            Map<JComponent, GBC> constraints = new HashMap<JComponent, GBC>();
            int buttonsInset = 2;
            constraints.put(localPanel, new GBC(0, 0, 1, 2)
                    .fill(GridBagConstraints.BOTH)
                    .weight(1, 1));
            constraints.put(remotePanel, new GBC(2, 0, 1, 2)
                    .fill(GridBagConstraints.BOTH)
                    .weight(1, 1));
            constraints.put(remoteToLocal, new GBC(1, 0, 1, 1)
                    .anchor(GridBagConstraints.SOUTH)
                    .insets(buttonsInset)
                    .weight(0, 1));
            constraints.put(localToRemote, new GBC(1, 1, 1, 1)
                    .anchor(GridBagConstraints.NORTH)
                    .insets(buttonsInset)
                    .weight(0, 1));
            for(Map.Entry<JComponent, GBC> e : constraints.entrySet()) {
                p.add(e.getKey(), e.getValue());
            }
        }
        mainFrame.add(p, BorderLayout.CENTER);
        p = new JPanel(new BorderLayout()); {
            JPanel q = new JPanel(); {
                q.add(asciiRadio);
                q.add(binaryRadio);
                q.add(autoBox);
            }
            p.add(q, BorderLayout.NORTH);
            JScrollPane sp = new JScrollPane(); {
                sp.setBorder(BorderFactory.createEtchedBorder());
                sp.setViewportView(console);
            }
            p.add(sp, BorderLayout.CENTER);
            q = new JPanel(new GridLayout(1,0)); {
                for(JButton b : statusButtons.values()) {
                    q.add(b);
                }
            }
            p.add(q, BorderLayout.SOUTH);
        }
        mainFrame.add(p, BorderLayout.SOUTH);
    }

    private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        {
            ButtonGroup bg = new ButtonGroup();
            bg.add(asciiRadio);
            bg.add(binaryRadio);
        }
    }

    public void display() {
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientFTP().display();
            }
        });
    }
}
