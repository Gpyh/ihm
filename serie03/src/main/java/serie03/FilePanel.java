package serie03;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.EnumMap;
import java.util.Map;

/**
 * Created by gpyh on 2/11/16.
 */
public class FilePanel extends JPanel {

    private final static int INDEX_FILTER = 2;
    private final static int[] COLUMN_WIDTHS =
            { FtpTableModel.ICON_WIDTH + 10, 130, 75, 50 };
    private final static int TABLE_HEIGHT = 100;

    private String title;
    private String rep;

    private Map<Action, JButton> actionButtons;
    private JTextField filterField;
    private JComboBox directoryComboBox;
    private JTable fileTable;

    public enum Action {
        CHGDIR("ChgDir"),
        MKDIR("MkDir"),
        VIEW("View"),
        EXEC("Exec"),
        RENAME("Rename"),
        DELETE("Delete"),
        REFRESH("Refresh"),
        DIRINFO("DirInfo");

        private String label;

        Action(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    public FilePanel(String title, String rep) {
        super(new GridBagLayout());
        this.title = title;
        this.rep = rep;
        createComponents();
        placeComponents();
        createBorder();
    }

    public Map<Action, JButton> getActionButtons() {
        return actionButtons;
    }

    public JTextField getFilterField() {
        return filterField;
    }

    public JComboBox getDirectoryComboBox() {
        return directoryComboBox;
    }

    public JTable getFileTable() {
        return fileTable;
    }

    private void createComponents() {
        actionButtons = new EnumMap<Action, JButton>(Action.class);
        for(Action a : Action.values()) {
            actionButtons.put(a, new JButton(a.toString()));
        }
        filterField = new JTextField();
        directoryComboBox = new JComboBox();
        directoryComboBox.addItem(rep);
        fileTable = createTable(rep);
    }

    private void placeComponents() {
        add(directoryComboBox, new GBC(0, 0, 2, 1)
            .fill(GridBagConstraints.HORIZONTAL));
        JScrollPane sp = new JScrollPane(); {
            sp.setViewportView(fileTable);
        }
        add(sp, new GBC(0, 1, 1, GridBagConstraints.REMAINDER)
            .fill(GridBagConstraints.BOTH));
        Action[] actions = Action.values();
        int s = actions.length;
        int i = 0;
        for(; i < 2; i++) {
            add(actionButtons.get(actions[i]), new GBC(1, i+1, 1, 1));
        }
        add(filterField, new GBC(1, i+1, 1, 1));
        for(; i < s; i++) {
            add(actionButtons.get(actions[i]), new GBC(1, i+2, 1, 1));
        }
        add(new JLabel(), new GBC(1, i+3, 1, 1)
            .weight(0, 1));

    }

    private void createBorder() {
        this.setBorder(BorderFactory.createTitledBorder(this.title));
    }

    private JTable createTable(String rep) {
        JTable table = new JTable(new FtpTableModel(rep));
        table.setShowGrid(false);
        table.setRowHeight(FtpTableModel.ICON_HEIGHT + 1);
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        int width = 0;
        for (int w : COLUMN_WIDTHS) {
            width += w;
        }
        table.setPreferredScrollableViewportSize(
                new Dimension(width, TABLE_HEIGHT)
        );
        for (int i = 0; i < COLUMN_WIDTHS.length; i++) {
            TableColumn c = table.getColumnModel().getColumn(i);
            c.setPreferredWidth(COLUMN_WIDTHS[i]);
        }
        return table;
    }

}
