package serie02;

import java.beans.*;
import java.util.*;

/**
 * Created by gpyh on 2/4/16.
 */
public class StdPodiumManager<E extends Drawable> implements PodiumManager<E> {

    private static Map<Order, Command> commands;
    PropertyChangeSupport propertyChangeListeners;
    VetoableChangeSupport vetoableChangeListeners;

    static {
        commands = new EnumMap<Order, Command>(Order.class);
        commands.put(Order.LO, new Command() {
            @Override
            public <T extends Drawable> void execute(StdPodiumManager<T> pm) {
                PodiumModel<T> wl = pm.getPodiumModel(Rank.WRK_LEFT);
                PodiumModel<T> wr = pm.getPodiumModel(Rank.WRK_RIGHT);
                wr.addTop(wl.top());
                wl.removeTop();
            }
        });
        commands.put(Order.KI, new Command() {
            @Override
            public <T extends Drawable> void execute(StdPodiumManager<T> pm) {
                PodiumModel<T> wl = pm.getPodiumModel(Rank.WRK_LEFT);
                PodiumModel<T> wr = pm.getPodiumModel(Rank.WRK_RIGHT);
                wl.addTop(wr.top());
                wr.removeTop();
            }
        });
        commands.put(Order.MA, new Command() {
            @Override
            public <T extends Drawable> void execute(StdPodiumManager<T> pm) {
                PodiumModel<T> wl = pm.getPodiumModel(Rank.WRK_LEFT);
                wl.addTop(wl.bottom());
                wl.removeBottom();
            }
        });
        commands.put(Order.NI, new Command() {
            @Override
            public <T extends Drawable> void execute(StdPodiumManager<T> pm) {
                PodiumModel<T> wr = pm.getPodiumModel(Rank.WRK_RIGHT);
                wr.addTop(wr.bottom());
                wr.removeBottom();
            }
        });
        commands.put(Order.SO, new Command() {
            @Override
            public <T extends Drawable> void execute(StdPodiumManager<T> pm) {
                PodiumModel<T> wl = pm.getPodiumModel(Rank.WRK_LEFT);
                PodiumModel<T> wr = pm.getPodiumModel(Rank.WRK_RIGHT);
                T buf = wl.top();
                wl.removeTop();
                wr.removeTop();
                wr.addTop(buf);
            }
        });
    }

    private final Set<E> drawables;
    private List<Order> orders;
    private Map<Rank, Podium<E>> podiums;
    private long startTime;
    private long timeDelta;
    private boolean finished;

    private PodiumModel<E> getPodiumModel(Rank rank) {
        return this.podiums.get(rank).getModel();
    }

    private interface Command {
        <T extends Drawable> void execute(StdPodiumManager<T> pm);
    }

    public StdPodiumManager(Set<E> drawables) {
        if(drawables == null) {
            throw new AssertionError("drawables is not allowed to be null");
        }
        if(drawables.size() < 2) {
            throw new AssertionError("There must be at least two drawables");
        }
        this.drawables = drawables;
        podiums = new EnumMap<Rank, Podium<E>>(Rank.class);
        orders = new ArrayList<Order>();
        List<List<E>> lst = createRandomElements();
        lst.addAll(createRandomElements());
        for (Rank r : Rank.values()) {
            podiums.put(r, new Podium<E>(new StdPodiumModel<E>(
                    lst.get(r.ordinal()),
                    drawables.size())));
        }
        init();
    }

    @Override
    public Order getLastOrder() {
        return orders.isEmpty() ? null : orders.get(orders.size() - 1);
    }

    @Override
    public Map<Rank, Podium<E>> getPodiums() {
        return Collections.unmodifiableMap(podiums);
    }

    @Override
    public int getShotsNb() {
        return orders.size();
    }

    @Override
    public long getTimeDelta() {
        return timeDelta / 1000000000;
    }

    private void checkFinished() {
        finished = getPodiumModel(Rank.WRK_LEFT)
                            .equals(getPodiumModel(Rank.OBJ_LEFT)) &&
                   getPodiumModel(Rank.WRK_RIGHT)
                            .equals(getPodiumModel(Rank.OBJ_RIGHT));
        if(finished) {
            timeDelta = System.nanoTime() - startTime;
        }
    }

    @Override
    public boolean isFinished() {
       return finished;
    }


    @Override
    public void addPropertyChangeListener(String propName, PropertyChangeListener lst) {
        if(lst == null) {
            throw new AssertionError("propertyChangeListener is not allowed to be null");
        }
        propertyChangeListeners.addPropertyChangeListener(propName, lst);
    }

    @Override
    public void addVetoableChangeListener(VetoableChangeListener lst) {
        if(lst == null) {
            throw new AssertionError("vetoableChangeListener is not allowed to be null");
        }
        vetoableChangeListeners.addVetoableChangeListener(lst);
    }

    @Override
    public void executeOrder(Order order) throws PropertyVetoException {
        Command command = commands.get(order);
        if(command == null) {
            throw new UnsupportedOperationException("Ordre non implémenté");
        }
        command.execute(this);
        checkFinished();
    }

    private void init() {
        timeDelta = 0;
        finished = false;
        startTime = System.nanoTime();
    }

    @Override
    public void reinit() {
        orders.clear();
        List<List<E>> lst = createRandomElements();
        lst.addAll(createRandomElements());
        for (Rank r : Rank.values()) {
            podiums.get(r).setModel(
                    new StdPodiumModel<E>(
                            lst.get(r.ordinal()),
                            drawables.size()
                    )
            );
        }
        init();
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener lst) {
        if(lst == null) {
            throw new AssertionError("propertyChangeListener is not allowed to be null");
        }
        propertyChangeListeners.removePropertyChangeListener(lst);
    }

    @Override
    public void removeVetoableChangeListener(VetoableChangeListener lst) {
        if(lst == null) {
            throw new AssertionError("vetoableChangeListener is not allowed to be null");
        }
        vetoableChangeListeners.removeVetoableChangeListener(lst);
    }

    private List<List<E>> createRandomElements() {
        final double ratio = 0.5;
        List<E> elements = new LinkedList<E>(drawables);
        List<E> list = new ArrayList<E>(drawables.size());
        for (int i = drawables.size(); i > 0; i--) {
            int k = ((int) (Math.random() * i));
            list.add(elements.get(k));
            elements.remove(k);
        }
        List<E> elemsL = new ArrayList<E>(drawables.size());
        List<E> elemsR = new ArrayList<E>(drawables.size());
        for (E e : list) {
            if (Math.random() < ratio) {
                elemsL.add(e);
            } else {
                elemsR.add(e);
            }
        }
        ArrayList<List<E>> result = new ArrayList<List<E>>(2);
        result.add(elemsL);
        result.add(elemsR);
        return result;
    }
}
