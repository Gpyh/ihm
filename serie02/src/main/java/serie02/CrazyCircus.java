package serie02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by gpyh on 2/11/16.
 */
public class CrazyCircus {

    private final static String NEW_GAME_LABEL = "Nouvelle partie";

    private JFrame mainFrame;
    private Map<PodiumManager.Order, JButton> orderButtons;
    private JButton newGameButton;
    private PodiumManager<AnimalColor> podiumManager;
    private JTextArea console;

    public CrazyCircus() {
        createModel();
        createView();
        placeComponents();
        createController();
    }

    private void createModel() {
        // TODO
    }

    private void createView() {
        mainFrame = new JFrame();
        newGameButton = new JButton(NEW_GAME_LABEL);
        orderButtons = new EnumMap<PodiumManager.Order, JButton>(PodiumManager.Order.class);
        for(PodiumManager.Order o : PodiumManager.Order.values()) {
            orderButtons.put(o, new JButton(o.toString()));
        }
        console = new JTextArea();
        console.setBorder(BorderFactory.createEtchedBorder());
        // TODO right numbers
        Set<AnimalColor> animals = EnumSet.allOf(AnimalColor.class);
        podiumManager = new StdPodiumManager<AnimalColor>(animals);
    }

    private void placeComponents() {
        Map<PodiumManager.Rank, Podium<AnimalColor>> podiums = podiumManager.getPodiums();
        JPanel p = new JPanel(new BorderLayout()); {
           JPanel q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(new GridLayout(0,2)); {
                    r.add(podiums.get(PodiumManager.Rank.WRK_LEFT));
                    r.add(podiums.get(PodiumManager.Rank.WRK_RIGHT));
                }
                q.add(r, BorderLayout.CENTER);
                q.add(new JLabel("Départ"), BorderLayout.SOUTH);
            }
            p.add(q, BorderLayout.WEST);
            q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(new GridLayout(0,2)); {
                    r.add(podiums.get(PodiumManager.Rank.OBJ_LEFT));
                    r.add(podiums.get(PodiumManager.Rank.OBJ_RIGHT));
                }
                q.add(r, BorderLayout.CENTER);
                q.add(new JLabel("Objectif"), BorderLayout.SOUTH);
            }
            p.add(q, BorderLayout.EAST);
        }
        mainFrame.add(p, BorderLayout.CENTER);
        p = new JPanel(new GridLayout(0, 1)); {
            for(JButton b : orderButtons.values()) {
                p.add(b);
            }
            // TODO CheckBox
            p.add(newGameButton);
        }
        mainFrame.add(p, BorderLayout.EAST);
        mainFrame.add(console, BorderLayout.SOUTH);
    }

    private class ActionListenerOrder implements ActionListener {

        private PodiumManager.Order order;

        ActionListenerOrder(PodiumManager.Order order) {
            this.order = order;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                podiumManager.executeOrder(order);
            } catch (PropertyVetoException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        for(Map.Entry<PodiumManager.Order, JButton> entry : orderButtons.entrySet()) {
            entry.getValue().addActionListener(new ActionListenerOrder(entry.getKey()));
        }
    }

    public void display() {
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    public static void main(String[] argv) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CrazyCircus().display();
            }
        });
    }
}
