package serie02;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import java.util.ArrayList;
import java.util.List;

/**
 * Impl�mentation standard de PodiumModel.
 */
public class StdPodiumModel<E> implements PodiumModel<E> {

    // ATTRIBUTS
    
    private List<E> data;
    private final int capacity;
    private EventListenerList listeners;

    // CONSTRUCTEURS
    
    public StdPodiumModel(List<E> init, int capacity) {
        if (init == null) {
            throw new AssertionError();
        }
        if (containsNullValue(init)) {
            throw new AssertionError();
        }
        
        this.capacity = capacity;
        data = new ArrayList<E>(init);
        listeners = new EventListenerList();
    }
    
    public StdPodiumModel() {
        this(new ArrayList<E>(), 0);
    }

    // REQUETES
    
    public E bottom() {
        if (size() <= 0) {
            throw new AssertionError();
        }
        
        return data.get(0);
    }
    
    public E elementAt(int i) {
        if (i < 0 || i >= capacity()) {
            throw new AssertionError();
        }
        
        if (i < size()) {
            return data.get(i);
        } else {
            return null;
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if ((o == null) || (o.getClass() != getClass())) {
            return false;
        }
        @SuppressWarnings("unchecked")
        StdPodiumModel<E> arg = (StdPodiumModel<E>) o;
        boolean result = (arg.capacity == capacity
                && data.size() == arg.data.size());
        for (int i = 0; result && (i < data.size()); i++) {
            result = data.get(i).equals(arg.data.get(i));
        }
        return result;
    }
    
    public int capacity() {
        return capacity;
    }
    
    public int size() {
        return data.size();
    }
    
    public E top() {
        if (size() <= 0) {
            throw new AssertionError();
        }
        
        return data.get(data.size() - 1);
    }

    @Override
    public void addChangeListener(ChangeListener cl) {
        if(cl == null) {
            throw new AssertionError("The changeListener it not allowed to be null");
        }
        listeners.add(ChangeListener.class, cl);
        fireStateChanged();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + capacity;
        result = prime * result + data.hashCode();
        return result;
    }
    
    @Override
    public String toString() {
        String res = "[";
        for (int i = 0; i < size(); i++) {
            res += data.get(i) + "|";
        }
        return capacity + "/" + res + "]";
    }

    // COMMANDES
    
    public void addTop(E elem) {
        if (elem == null) {
            throw new AssertionError();
        }
        if (size() >= capacity()) {
            throw new AssertionError("la structure est pleine");
        }
        
        data.add(elem);
    }
    
    public void removeBottom() {
        if (size() <= 0) {
            throw new AssertionError("la structure est vide");
        }
        
        data.remove(0);
        fireStateChanged();
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
        if(cl == null) {
            throw new AssertionError("The changeListener it not allowed to be null");
        }
        listeners.remove(ChangeListener.class, cl);
        fireStateChanged();
    }

    public void removeTop() {
        if (size() <= 0) {
            throw new AssertionError("la structure est vide");
        }
        
        data.remove(size() - 1);
    }

    // OUTILS
    
    private boolean containsNullValue(List<E> list) {
        for (E e : list) {
            if (e == null) {
                return true;
            }
        }
        return false;
    }

    private void fireStateChanged() {
        ChangeEvent evt = new ChangeEvent(this);
        for(ChangeListener cl : listeners.getListeners(ChangeListener.class)) {
            cl.stateChanged(evt);
        }
    }
}
