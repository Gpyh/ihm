package serie01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.*;

public class EventsTester {

    private static final String CREATE_FRAME_LABEL = "Nouvelle fenêtre";
    private static final String RAZ_COUNTER_LABEL = "RAZ Compteur";

    private static Map<Class, Class[]> eventListenerMap = new HashMap<Class, Class[]>();
    static {
        eventListenerMap.put(PropertyChangeEvent.class, new Class[]{PropertyChangeListener.class});
        eventListenerMap.put(WindowEvent.class, new Class[]{WindowListener.class, WindowStateListener.class, WindowFocusListener.class});
        eventListenerMap.put(ContainerEvent.class, new Class[]{ContainerListener.class});
        eventListenerMap.put(ComponentEvent.class, new Class[]{ComponentListener.class});
        eventListenerMap.put(FocusEvent.class, new Class[]{FocusListener.class});
        eventListenerMap.put(HierarchyEvent.class, new Class[]{HierarchyListener.class, HierarchyBoundsListener.class});
        eventListenerMap.put(KeyEvent.class, new Class[]{KeyListener.class});
        eventListenerMap.put(MouseEvent.class, new Class[]{MouseListener.class, MouseMotionListener.class});
        eventListenerMap.put(MouseWheelEvent.class, new Class[]{MouseWheelListener.class});
        eventListenerMap.put(InputMethodEvent.class, new Class[]{InputMethodListener.class});
    }

    private Set<Class> listenedEvents;

    private int eventsCount = 0;
    private int razCount = 0;

    private JFrame mainFrame;
    private JButton createFrame;
    private JButton razCounter;
    private JFrame testFrame;
    private Map<Class, JTextArea> eventLoggers;
    private FrameEventsListener testFrameEventsListener;

    public EventsTester() {
        this.listenedEvents = new HashSet<Class>(4);
        listenedEvents.add(WindowEvent.class);
        listenedEvents.add(KeyEvent.class);
        listenedEvents.add(MouseEvent.class);
        listenedEvents.add(MouseWheelEvent.class);
        testFrame = null;
        createView();
        placeComponents();
        createControllers();
    }

    private void createView() {
        mainFrame = new JFrame();
        createFrame = new JButton(CREATE_FRAME_LABEL);
        razCounter = new JButton(RAZ_COUNTER_LABEL);
        eventLoggers = new HashMap<Class, JTextArea>();
        for(Class eventClass : listenedEvents) {
            for(Class listenerClass : eventListenerMap.get(eventClass)) {
                eventLoggers.put(listenerClass, new JTextArea());
            }
        }
    }

    private void placeComponents() {
        JPanel p = new JPanel(); {
            p.add(createFrame);
            p.add(razCounter);
        }
        mainFrame.add(p, BorderLayout.NORTH);
        p = new JPanel(new GridLayout(3, 0)); {
            JScrollPane q;
            for(Map.Entry<Class, JTextArea> e : eventLoggers.entrySet()) {
                q = new JScrollPane(e.getValue());
                q.setBorder(BorderFactory.createTitledBorder(e.getKey().getSimpleName()));
                p.add(q);
                q.setPreferredSize(new Dimension(300, 200));
            }
        }
        mainFrame.add(p, BorderLayout.CENTER);
    }

    private void createControllers() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        createFrame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createNewTestFrame();
            }
        });

        razCounter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eventsCount = 0;
                razCount++;
                for(JTextArea logArea : eventLoggers.values()) {
                    logArea.append("--- RAZ " + razCount + "---\n");
                }
            }
        });

        testFrameEventsListener = new FrameEventsListener() {
            @Override
            public void eventDispatched(EventObject e, Class listenerClass) {
                System.out.println("YOLO");
                if(listenedEvents.contains(e.getClass())) {
                    if(e instanceof AWTEvent) {
                        String nameEvent = ((AWTEvent) e).paramString();
                        nameEvent.substring(0,nameEvent.indexOf(','));
                    }
                    eventLoggers.get(listenerClass).append((++eventsCount) + " " + e.getClass().getSimpleName() + "\n");
                }
            }
        };
    }

    private void createNewTestFrame() {
        if(testFrame != null) {
            testFrame.dispose();
            eventsCount = 0;
            razCount = 0;
        }
        testFrame = new JFrame();
        testFrame.setPreferredSize(new Dimension(200, 100));
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);

        testFrame.addPropertyChangeListener(testFrameEventsListener);
        testFrame.addWindowListener(testFrameEventsListener);
        testFrame.addWindowStateListener(testFrameEventsListener);
        testFrame.addWindowFocusListener(testFrameEventsListener);
        testFrame.addContainerListener(testFrameEventsListener);
        testFrame.addComponentListener(testFrameEventsListener);
        testFrame.addFocusListener(testFrameEventsListener);
        testFrame.addHierarchyListener(testFrameEventsListener);
        testFrame.addHierarchyBoundsListener(testFrameEventsListener);
        testFrame.addKeyListener(testFrameEventsListener);
        testFrame.addMouseListener(testFrameEventsListener);
        testFrame.addMouseMotionListener(testFrameEventsListener);
        testFrame.addMouseWheelListener(testFrameEventsListener);
        testFrame.addInputMethodListener(testFrameEventsListener);
    }

    public void display() {
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    public static void main(String[] argv) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new EventsTester().display();
            }
        });
    }
}