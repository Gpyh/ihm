package serie01;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventObject;

public abstract class FrameEventsListener implements
        PropertyChangeListener,
        WindowListener,
        WindowStateListener,
        WindowFocusListener,
        ContainerListener,
        ComponentListener,
        FocusListener,
        HierarchyListener,
        HierarchyBoundsListener,
        KeyListener,
        MouseListener,
        MouseMotionListener,
        MouseWheelListener,
        InputMethodListener {

    public abstract void eventDispatched(EventObject e, String eventName, Class listenerClass);

    @Override
    public void componentResized(ComponentEvent e) {
        eventDispatched(e, ComponentListener.class);
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        eventDispatched(e, ComponentListener.class);
    }

    @Override
    public void componentShown(ComponentEvent e) {
        eventDispatched(e, ComponentListener.class);
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        eventDispatched(e, ComponentListener.class);
    }

    @Override
    public void componentAdded(ContainerEvent e) {
        eventDispatched(e, ContainerListener.class);
    }

    @Override
    public void componentRemoved(ContainerEvent e) {
        eventDispatched(e, ContainerListener.class);
    }

    @Override
    public void focusGained(FocusEvent e) {
        eventDispatched(e, FocusListener.class);
    }

    @Override
    public void focusLost(FocusEvent e) {
        eventDispatched(e, FocusListener.class);
    }

    @Override
    public void ancestorMoved(HierarchyEvent e) {
        eventDispatched(e, HierarchyBoundsListener.class);
    }

    @Override
    public void ancestorResized(HierarchyEvent e) {
        eventDispatched(e, HierarchyBoundsListener.class);
    }

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        eventDispatched(e, HierarchyListener.class);
    }

    @Override
    public void inputMethodTextChanged(InputMethodEvent event) {
        eventDispatched(event, InputMethodListener.class);
    }

    @Override
    public void caretPositionChanged(InputMethodEvent event) {
        eventDispatched(event, InputMethodListener.class);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        eventDispatched(e, KeyListener.class);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        eventDispatched(e, KeyListener.class);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        eventDispatched(e, KeyListener.class);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        eventDispatched(e, MouseListener.class);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        eventDispatched(e, MouseListener.class);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        eventDispatched(e, MouseListener.class);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        eventDispatched(e, MouseListener.class);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        eventDispatched(e, MouseListener.class);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        eventDispatched(e, MouseMotionListener.class);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        eventDispatched(e, MouseMotionListener.class);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        eventDispatched(e, MouseWheelListener.class);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        eventDispatched(evt, PropertyChangeListener.class);
    }

    @Override
    public void windowGainedFocus(WindowEvent e) {
        eventDispatched(e, WindowFocusListener.class);
    }

    @Override
    public void windowLostFocus(WindowEvent e) {
        eventDispatched(e, WindowFocusListener.class);
    }

    @Override
    public void windowOpened(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowClosed(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowIconified(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowActivated(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        eventDispatched(e, WindowListener.class);
    }

    @Override
    public void windowStateChanged(WindowEvent e) {
        eventDispatched(e, WindowStateListener.class);
    }
}
